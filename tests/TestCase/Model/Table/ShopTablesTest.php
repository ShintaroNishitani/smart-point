<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ShopTables;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ShopTables Test Case
 */
class ShopTablesTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ShopTables
     */
    public $ShopTables;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Shops') ? [] : ['className' => ShopTables::class];
        $this->ShopTables = TableRegistry::getTableLocator()->get('Shops', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ShopTables);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
