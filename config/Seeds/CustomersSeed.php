<?php
use Migrations\AbstractSeed;

/**
 * Customers seed.
 */
class CustomersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'shop_id' => '1',
                'name' => '顧客A',
                'point' => '29',
                'enable' => '1',
                'created' => '2020-10-20 15:52:07',
                'modified' => '2020-10-23 11:30:00',
            ],
            [
                'id' => '2',
                'shop_id' => '1',
                'name' => '顧客B',
                'point' => '0',
                'enable' => '1',
                'created' => '2020-10-13 10:55:02',
                'modified' => '2020-10-13 10:55:02',
            ],
            [
                'id' => '3',
                'shop_id' => '1',
                'name' => '顧客C',
                'point' => '2',
                'enable' => '1',
                'created' => '2020-10-13 16:08:39',
                'modified' => '2020-10-23 11:38:59',
            ],
            [
                'id' => '31',
                'shop_id' => '1',
                'name' => '',
                'point' => '0',
                'enable' => '0',
                'created' => '2020-10-23 13:24:17',
                'modified' => '2020-10-23 13:24:49',
            ],
            [
                'id' => '32',
                'shop_id' => '1',
                'name' => '',
                'point' => '0',
                'enable' => '0',
                'created' => '2020-10-23 13:24:51',
                'modified' => '2020-10-23 13:25:06',
            ],
            [
                'id' => '33',
                'shop_id' => '1',
                'name' => '',
                'point' => '0',
                'enable' => '0',
                'created' => '2020-10-23 13:25:08',
                'modified' => '2020-10-23 13:25:24',
            ],
            [
                'id' => '34',
                'shop_id' => '1',
                'name' => 'aaa',
                'point' => '1',
                'enable' => '0',
                'created' => '2020-10-23 13:25:27',
                'modified' => '2020-10-23 13:52:19',
            ],
            [
                'id' => '35',
                'shop_id' => '1',
                'name' => '',
                'point' => '0',
                'enable' => '0',
                'created' => '2020-10-23 13:25:54',
                'modified' => '2020-10-23 13:27:33',
            ],
            [
                'id' => '36',
                'shop_id' => '1',
                'name' => '',
                'point' => '1',
                'enable' => '0',
                'created' => '2020-10-23 13:27:36',
                'modified' => '2020-10-23 13:29:19',
            ],
            [
                'id' => '37',
                'shop_id' => '1',
                'name' => '',
                'point' => '100',
                'enable' => '0',
                'created' => '2020-10-23 13:27:56',
                'modified' => '2020-10-23 13:29:16',
            ],
            [
                'id' => '38',
                'shop_id' => '1',
                'name' => '',
                'point' => '1',
                'enable' => '0',
                'created' => '2020-10-23 13:28:24',
                'modified' => '2020-10-23 13:29:13',
            ],
            [
                'id' => '39',
                'shop_id' => '1',
                'name' => '',
                'point' => '0',
                'enable' => '0',
                'created' => '2020-10-23 13:29:27',
                'modified' => '2020-10-23 13:51:30',
            ],
            [
                'id' => '40',
                'shop_id' => '1',
                'name' => '',
                'point' => '0',
                'enable' => '0',
                'created' => '2020-10-23 13:30:08',
                'modified' => '2020-10-23 13:51:27',
            ],
            [
                'id' => '41',
                'shop_id' => '1',
                'name' => '',
                'point' => '0',
                'enable' => '0',
                'created' => '2020-10-23 13:51:34',
                'modified' => '2020-10-23 13:52:16',
            ],
            [
                'id' => '42',
                'shop_id' => '1',
                'name' => '',
                'point' => '2',
                'enable' => '0',
                'created' => '2020-10-23 13:51:52',
                'modified' => '2020-10-23 13:52:13',
            ],
        ];

        $table = $this->table('customers');
        $table->insert($data)->save();
    }
}
