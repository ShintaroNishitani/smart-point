<?php
use Migrations\AbstractSeed;

/**
 * Accounts seed.
 */
class AccountsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'shop_id' => '1',
                'name' => 'オーシャン太郎',
                'mail' => 'ocean',
                'password' => '$2y$10$Gh8ii3lSQjdCs.bhttxZWOn5HwEwHY3VyXMjbWKepNeOaodMKQ1Ku',
                'enable' => '1',
                'created' => '2020-07-01 15:38:41',
                'modified' => '2020-07-01 15:38:41',
            ],
        ];

        $table = $this->table('accounts');
        $table->insert($data)->save();
    }
}
