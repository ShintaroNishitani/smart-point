<?php
use Migrations\AbstractSeed;

/**
 * Shops seed.
 */
class ShopsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => '風林火山',
                'tel' => '06-6390-3344',
                'zip' => '532-0011',
                'address1' => '大阪市淀川区西中島4-4-11',
                'address2' => '',
                'point_rate' => '1000',
                'remark' => '1000円で1ポイント進呈
★40ポイントで
①ボトル１本進呈
または
②会計2000円OFF
次回よりご利用頂けます

営業時間17:00～0:00
日曜定休日',
                'enable' => '1',
                'created' => NULL,
                'modified' => '2020-10-23 10:42:36',
            ],
        ];

        $table = $this->table('shops');
        $table->insert($data)->save();
    }
}
