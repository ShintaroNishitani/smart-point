<div class="row">
    <div class="col-lg-6">
        <div class="card card-pricing "  style="background-color:#f4f3ef;">
            <div class="card-header" style="padding:0px;margin:0px;">
                <h4 class="card-title">
                <i class="fa fa-address-card-o"></i> ポイントカード<br/>
                </h4>
            </div>
            <hr>
            <div class="card-footer">
                    <?= $this->Form->create($customer, ['url' => ['action' =>'pointSet']]) ?>
                        <?= $this->Form->hidden('id', ["id"=>"id"]);?>
                        <div class="card card-user">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-12 col-sm-12">
                                        <div class="form-group">
                                            <label>QRコードをスキャンしてください</label>
                                            <?=$this->QrCode->text($url);?>
                                        </div>
                                        <div class="form-group">
                                            <label>名前</label>
                                            <?= $this->Form->control('name', ["class"=>"form-control", "label"=>false]);?>
                                        </div>
                                        <div class="form-group">
                                            <label>金額</label>
                                            <?= $this->Form->control('price', ["class"=>"form-control", "label"=>false]);?>
                                        </div>
                                    </div>
                                </div>
                                <?= $this->Form->button(' '.__('登録'), ["type"=>"submit", "class"=>"btn btn-primary"]) ?>
                                <button type="button" class="btn btn-warning" onclick="javascript:window.close();">
                                    閉じる
                                </button>
                            </div>
                        </div>
                    <?= $this->Form->end() ?>

            </div>
        </div>
    </div>
</div>
