<div class="row">
    <div class="col-lg-6">
        <div class="card card-pricing "  style="background-color:#f4f3ef;">
            <div class="card-header" style="padding:0px;margin:0px;">
                <h4 class="card-title">
                <i class="fa fa-address-card-o"></i> ポイントカード<br/>
                    <?= h($customer->shop->name) ?>
                </h4>
            </div>
            <div class="card-body">
                <h5 class="card-title"><?= h($customer->name.'様') ?></h5>
                <ul>
                    <li><?= h("発行日：".$customer->created->format('Y年m月d日')) ?></li>
                    <li><?= h("現在：".$customer->point.'ポイント') ?></li>
                </ul>
            </div>
            <hr>
            <div class="card-footer">
                <?php if (!$complete): ?>
                    <?= $this->Form->create($customer, ['onsubmit' => "return confirm(\"ポイントを付与しますか？ \");"]) ?>
                        <div class="card card-user">
                            <div class="card-body">
                                <?php if ($customer->shop->point_rate): ?>
                                    <div class="row">
                                        <div class="col-xl-12 col-sm-12">
                                            <div class="form-group">
                                            <label>金額を入力してください</label>
                                            <?= $this->Form->control('price', ["class"=>"form-control text-right", "label"=>false]);?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?= $this->Form->button(' '.__('ポイント付与'), ["type"=>"submit", "class"=>"btn btn-primary btn-round"]) ?>
                            </div>
                        </div>
                    <?= $this->Form->end() ?>
                <?php else: ?>
                    <button type="button" class="btn btn-warning btn-sm" onclick="javascript:window.close();">
                        閉じる
                    </button>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
