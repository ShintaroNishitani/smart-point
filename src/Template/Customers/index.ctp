<script>
    function deleteData(id) {
        Swal.fire({
            title: '',
            text: "顧客を削除しますか?",
            type: 'warning',
            showCancelButton: true,
            customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false,
            confirmButtonText: '削除',
            cancelButtonText: 'キャンセル'
        }).then((result) => {
            if (result.value) {
                location.href = "/customers/delete/" + id;
            }
        })
    }
    function editData(id) {
        $.ajax({
            type: "get",
            dataType: 'json',
            url: "/customers/edit/" + id,
            success : function(data){
                if (data && data.data){
                    console.log(data.data)
                    $('#id').val(data.data.id);
                    $('#name').val(data.data.name);
                    $('#point').val(data.data.point);

                    $('#addModal').modal()
                }
            }
        });
    }
</script>
<div class="row">
    <div class="col-xl-10 col-sm-12">
        <div class="card card-user">
            <div class="card-body">
                <?= $this->Html->link('<i class="fa fa-plus"></i> 新規発行', ['action'=>'add'], ['escape'=>false, "class"=>"btn btn-outline-success btn-round", 'target'=>'_blank']) ?>
                <table class="table table-bordered table-striped text-nowrap">
                    <thead>
                        <tr>
                            <th class="text-center" scope="col" class="actions"><?= __('ポイントカード') ?></th>
                            <th class="text-center" scope="col"><?= $this->Paginator->sort('名前') ?></th>
                            <th class="text-center" scope="col"><?= $this->Paginator->sort('ポイント') ?></th>
                            <th class="text-center" scope="col" class="actions"><?= __('') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($customers as $customer): ?>
                        <tr>
                            <td class="text-center">
                                <?= $this->Html->link('再発行', ['action'=>'view', $customer->id], ['escape'=>false, "class"=>"btn btn-sm  btn-behance", 'target'=>'_blank']) ?>
                            </td>
                            <td><?= h($customer->name) ?></td>
                            <td><?= h($customer->point."P") ?></td>
                            <td class="text-center">
                                <button class="btn btn-sm btn-icon btn-round btn-success" onclick="editData(<?= h($customer->id) ?>)">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button class="btn btn-sm btn-icon btn-round btn-reddit" onclick="deleteData(<?= h($customer->id) ?>)">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="paginator">
                    <ul class="pagination">
                        <?= $this->Paginator->first('<< ' . __('first')) ?>
                        <?= $this->Paginator->prev('< ' . __('前')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('次') . ' >') ?>
                        <?= $this->Paginator->last(__('last') . ' >>') ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-address-card-o" aria-hidden="true"></i> 顧客登録</h5>
      </div>
      <?= $this->Form->create() ?>
          <?= $this->Form->hidden('id', ["id"=>"id"]);?>

        <div class="modal-body">
            <div class="form-group">
                <label>名前</label>
                <?= $this->Form->control('name', ["class"=>"form-control", "label"=>false]);?>
            </div>
            <div class="form-group">
                <label>ポイント</label>
                <?= $this->Form->control('point', ["class"=>"form-control", "label"=>false]);?>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">閉じる</button>
          <?= $this->Form->button(__('登録'), ["type"=>"submit", "class"=>"btn btn-success"]) ?>
        </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>
