<script>

</script>
<div class="row">
    <div class="col-xl-10 col-sm-12">
        <?= $this->Form->create($shop) ?>
        <div class="card card-user">
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-6 col-sm-6">
                        <div class="form-group">
                        <label>店舗名</label>
                        <?= $this->Form->control('name', ["class"=>"form-control", "label"=>false]);?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-sm-3">
                        <div class="form-group">
                        <label>電話番号</label>
                        <?= $this->Form->control('tel', ["class"=>"form-control", "label"=>false]);?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-2 col-sm-4">
                        <div class="form-group">
                        <label>郵便番号</label>
                        <?= $this->Form->control('zip', ["class"=>"form-control", "label"=>false]);?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-sm-12">
                        <div class="form-group">
                        <label>住所１</label>
                        <?= $this->Form->control('address1', ["class"=>"form-control", "label"=>false]);?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-sm-12">
                        <div class="form-group">
                        <label>住所２</label>
                        <?= $this->Form->control('address2', ["class"=>"form-control", "label"=>false]);?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-2 col-sm-2">
                        <div class="form-group">
                        <label>ポイント割合<br/>(〇〇円で1ポイント)</label>
                        <?= $this->Form->control('point_rate', ["class"=>"form-control", "label"=>false]);?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-sm-12">
                        <div class="form-group">
                        <label>備考</label>
                        <?= $this->Form->textarea('remark', ["class"=>"form-control", "rows"=>10]);?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="update ml-auto mr-auto">
               <?= $this->Form->button(' '.__('変更内容を保存・確認'), ["type"=>"submit", "class"=>"btn btn-primary btn-round"]) ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>


