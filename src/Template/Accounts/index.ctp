<script>
    function deleteData(id) {
        Swal.fire({
            title: '',
            text: "アカウントを削除しますか?",
            type: 'warning',
            showCancelButton: true,
            customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false,
            confirmButtonText: '削除',
            cancelButtonText: 'キャンセル'
        }).then((result) => {
            if (result.value) {
                location.href = "/accounts/delete/" + id;
            }
        })
    }

    function editData(id) {
        $.ajax({
            type: "get",
            dataType: 'json',
            url: "/accounts/edit/" + id,
            success : function(data){
                if (data && data.data){
                    $('#id').val(data.data.id);
                    $('#name').val(data.data.name);
                    $('#mail').val(data.data.mail);
                    $('#password').val(data.data.password);

                    $('#addModal').modal()
                }
            }
        });
    }

</script>
<div class="row">
    <div class="col-xl-10 col-sm-12">
        <div class="card card-user">
            <div class="card-body">
                <button class="btn btn-outline-success btn-round" onclick="editData(0)"><span class="btn-label"><i class="fa fa-plus"></i></span>
                新規追加
                </button>
                <table class="table table-bordered table-striped table-responsive">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('名前') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('メール') ?></th>
                            <th scope="col" class="actions"><?= __('') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($accounts as $account): ?>
                        <tr>
                            <td><?= h($account->name) ?></td>
                            <td><?= h($account->mail) ?></td>
                            <td class="text-center">
                                <button class="btn btn-sm btn-icon btn-round btn-success" onclick="editData(<?= h($account->id) ?>)">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button class="btn btn-sm btn-icon btn-round btn-reddit" onclick="deleteData(<?= h($account->id) ?>)">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="paginator">
                    <ul class="pagination">
                        <?= $this->Paginator->first('<< ' . __('first')) ?>
                        <?= $this->Paginator->prev('< ' . __('前')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('次') . ' >') ?>
                        <?= $this->Paginator->last(__('last') . ' >>') ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">アカウント登録</h5>
      </div>
      <?= $this->Form->create() ?>
          <?= $this->Form->hidden('id', ["id"=>"id"]);?>

        <div class="modal-body">
            <div class="form-group">
                <label>アカウント名</label>
                <?= $this->Form->control('name', ["class"=>"form-control", "label"=>false]);?>
            </div>
            <div class="form-group">
                <label>メールアドレス</label>
                <?= $this->Form->control('mail', ["class"=>"form-control", "label"=>false]);?>
            </div>
            <div class="form-group">
                <label>パスワード</label>
                <?= $this->Form->control('password', ["class"=>"form-control", "label"=>false]);?>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">閉じる</button>
          <?= $this->Form->button(__('登録'), ["type"=>"submit", "class"=>"btn btn-success"]) ?>
        </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>
