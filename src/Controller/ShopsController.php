<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * Shops Controller
 *
 * @property \App\Model\Table\ShopsTable $Shops
 *
 * @method \App\Model\Entity\ShopTable[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ShopsController extends AppController
{
    /**
     * 店舗情報
     */
    public function edit()
    {
        $this->set('title', __('<i class="nc-icon nc-settings-gear-65"></i> 店舗情報'));

        $shop = $this->Shops->get($this->Auth->user()['shop_id'], [
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $shop = $this->Shops->patchEntity($shop, $this->request->getData());
            if ($this->Shops->save($shop)) {
                $this->Flash->success(__('更新されました。'));

            } else{
                $this->Flash->error(__('更新に失敗しました。'));
            }
        }

        $this->set(compact('shop'));

    }

}
