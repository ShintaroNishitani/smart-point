<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

/**
 * Accounts Controller
 *
 * @property \App\Model\Table\AccountsTable $Accounts
 *
 * @method \App\Model\Entity\Account[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AccountsController extends AppController
{
    /**
     * ログイン
     * @return \Cake\Http\Response|null
     */
    public function login()
    {
        $this->viewBuilder()->setLayout("none");
        $this->set('title', __('ログイン'));

        $tblAutoLogins = TableRegistry::get('AutoLogins');

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                // 自動ログインの場合は、ログインキーをDBに保存し、クッキーに書き込む
                if (!empty($this->request->getData('auto_login_check'))) {
                    $set_key = hash('sha256', (uniqid() . mt_rand(1, 999999999) . '_auto_logins'));
                    $entity = $tblAutoLogins->newEntity(['account_id' => $user['id'], 'login_key' => $set_key]);
                    $tblAutoLogins->save($entity);
                    $this->Cookie->write('AUTO_LOGIN', $set_key);
                }
                return $this->redirect(['controller' => 'Customers', 'action' => 'index']);
            }
            $this->Flash->error(__('ユーザ名もしくはパスワードが間違っています'));
        } else {
            // 自動ログインチェック
            $auto_login_key = $this->Cookie->read('AUTO_LOGIN');
            if (!empty($auto_login_key)) {
                // とりあえず1ヶ月有効に
                $auto_login = $tblAutoLogins->find()
                    ->where(['login_key' => $auto_login_key, 'created >=' => date('Y-m-d H:i:s', strtotime('-1 month'))])
                    ->first();
                if (isset($auto_login->account_id)) {
                    $this->loadModel('Accounts');
                    $user = $this->Accounts->find()->where(['id' => $auto_login->account_id])->first();
                    if (!empty($user)) {
                        $this->Auth->setUser($user);
                        return $this->redirect(['controller' => 'Customers', 'action' => 'index']);
                    }
                }
            }
        }
    }

    /**
     * ログアウト
     * @return \Cake\Http\Response|null
     */
    public function logout()
    {
        $tblAutoLogins = TableRegistry::get('AutoLogins');
        $tblAutoLogins->deleteAll(['account_id' => $this->Auth->user()['id']]);

        $this->Cookie->delete('AUTO_LOGIN');

        return $this->redirect($this->Auth->logout());

    }

    /**
     * アカウント一覧
     */
    public function index()
    {
        $this->set('title', __('<i class="nc-icon nc-circle-10"></i> アカウント管理'));

        $query = $this->Accounts->find()->where(['Accounts.shop_id'=>$this->Auth->user()['shop_id']]);
        $accounts = $this->paginate($query);

        if ($this->request->is(['patch', 'post', 'put'])) {
            if (!$this->request->getData('id')) {
                $data = $this->Accounts->newEntity();
            } else {
                $data = $this->Accounts->get($this->request->getData('id'));
            }
            $data = $this->Accounts->patchEntity($data, $this->request->getData());
            $data->shop_id = $this->Auth->user()['shop_id'];
            if ($this->Accounts->save($data)) {
                $this->Flash->success(__('アカウントが登録されました。'));
                return $this->redirect(['action' => 'index']);

            } else {
                $this->log($data->errors());
                $this->Flash->error(__('アカウントの登録に失敗しました。'));
            }

        }

        $this->set(compact('accounts'));
    }

    /**
     * アカウント追加
     */
    public function add()
    {
        $account = $this->Accounts->newEntity();
        if ($this->request->is('post')) {
            $account = $this->Accounts->patchEntity($account, $this->request->getData());
            if ($this->Accounts->save($account)) {
                $this->Flash->success(__('The account has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The account could not be saved. Please, try again.'));
        }
        $shops = $this->Accounts->Shops->find('list', ['limit' => 200]);
        $this->set(compact('account', 'shops'));
    }

    /**
     * アカウント登録
     */
    public function edit($id = null)
    {
        $this->autoRender = false;

        $data = $this->Accounts->newEntity();
        if ($id) {
            $data = $this->Accounts->get($id);
        }
        echo json_encode(compact('data'));
    }

    /**
     * アカウント削除
     */
    public function delete($id = null)
    {
        $data = $this->Accounts->get($id);
        $data->enable = 0;
        if ($this->Accounts->save($data)) {
            $this->Flash->success(__('アカウントを削除しました。'));
        } else {
            $this->Flash->error(__('アカウントの削除に失敗しました。'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
