<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\Routing\Router;


class CustomersController extends AppController
{

    /**
     * 顧客一覧
     */
    public function index()
    {
        $this->set('title', __('<i class="nc-icon nc-badge"></i> ポイントカード'));

        $query = $this->Customers->find()->where(['Customers.shop_id'=>$this->Auth->user()['shop_id']]);
        $customers = $this->paginate($query);

        if ($this->request->is(['patch', 'post', 'put'])) {
            if (!$this->request->getData('id')) {
                $data = $this->Customers->newEntity();
            } else {
                $data = $this->Customers->get($this->request->getData('id'));
            }
            $data = $this->Customers->patchEntity($data, $this->request->getData());
            $data->shop_id = $this->Auth->user()['shop_id'];
            if ($this->Customers->save($data)) {
                $this->Flash->success(__('顧客が登録されました。'));
                return $this->redirect(['action' => 'index']);

            } else {
                $this->log($data->errors());
                $this->Flash->error(__('顧客の登録に失敗しました。'));
            }

        }

        $this->set(compact('customers'));
    }

    /**
     * 新規登録
     */
    public function add()
    {
        $this->viewBuilder()->setLayout('none');

        $this->set('title', __('ポイントカード新規発行'));

        $customer = $this->Customers->newEntity();
        $customer->shop_id = $this->Auth->user()['shop_id'];
        if (!$this->Customers->save($customer)) {
            $this->log($customer->errors());
            $this->Flash->error(__('ポイントカードが発行に失敗しました。'));
        }

        $url = Router::url('/', true) . "customers/view/". $customer->id;

        $this->set(compact('customer', 'url'));
    }

    /**
     * 顧客登録
     */
    public function edit($id = null)
    {
        $this->autoRender = false;

        $data = $this->Customers->newEntity();
        if ($id) {
            $data = $this->Customers->get($id);
            if ($data->next_date) {
                $data->next_date = $data->next_date->format('Y-m-d');
            }
        }
        echo json_encode(compact('data'));
    }

    /**
     * 顧客削除
     */
    public function delete($id = null)
    {
        $data = $this->Customers->get($id);
        $data->enable = 0;
        if ($this->Customers->save($data)) {
            $this->Flash->success(__('顧客を削除しました。'));
        } else {
            $this->Flash->error(__('顧客の削除に失敗しました。'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * ポイントカード
     */
    public function view($id)
    {
        $this->viewBuilder()->setLayout('none');

        $this->set('title', __('ポイントカード'));

        $customer = $this->Customers->get($id, [
            'contain' => ['Shops'],
        ]);

        $url = Router::url('/', true) . "customers/point-set/". $id;

        $this->set(compact('customer', 'url'));
    }

    /**
     * ポイント付与
     */
    public function pointSet($id)
    {
        $this->viewBuilder()->setLayout('none');

        $this->set('title', __('ポイント付与'));

        $customer = $this->Customers->get($id, [
            'contain' => ['Shops'],
        ]);

        $complete = false;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $customer = $this->Customers->patchEntity($customer, $this->request->getData());
            // ポイント比率が設定されている場合は、金額を割ってポイント計算。設定されていない場合は、1ポイント付与。
            if ($customer->shop->point_rate) {
                if ($customer->price) {
                    $customer->point += floor($customer->price / $customer->shop->point_rate);
                }
            } else {
                $customer->point++;
            }
            if ($this->Customers->save($customer)) {
                $complete = true;
                $this->Flash->success(__('ポイントが付与されました。'));
            } else{
                $this->Flash->error(__('ポイントの付与に失敗しました。'));
            }
        }

        $this->set(compact('customer', 'complete'));
    }
}
