<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class AutoLogin extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
